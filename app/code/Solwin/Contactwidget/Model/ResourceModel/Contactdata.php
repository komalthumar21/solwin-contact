<?php
namespace Solwin\Contactwidget\Model\ResourceModel;

class Contactdata extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('contact_data', 'id');
	}
}