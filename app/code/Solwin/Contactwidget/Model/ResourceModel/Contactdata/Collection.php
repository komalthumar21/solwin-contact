<?php
namespace Solwin\Contactwidget\Model\ResourceModel\Contactdata;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected function _construct()
	{
		$this->_init('Solwin\Contactwidget\Model\Contactdata', 'Solwin\Contactwidget\Model\ResourceModel\Contactdata');
	}
}