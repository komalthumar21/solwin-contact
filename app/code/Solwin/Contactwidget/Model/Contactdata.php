<?php
namespace Solwin\Contactwidget\Model;

class Contactdata extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Solwin\Contactwidget\Model\ResourceModel\Contactdata');
    }
}