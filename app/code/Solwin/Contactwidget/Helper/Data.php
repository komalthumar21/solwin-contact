<?php
/**
 * Solwin Infotech
 * Solwin Contact Form Widget Extension
 *
 * @category   Solwin
 * @package    Solwin_Contactwidget
 * @copyright  Copyright © 2006-2020 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Contactwidget\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Get config value
     */
    public function getConfigValue($value = '')
    {
        return $this->scopeConfig
                ->getValue(
                    $value,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
    }

    /**
     * Get base url
     */
    public function getBaseUrl()
    {
        return $this->_storeManager
                ->getStore()
                ->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                );
    }

    /**
     * Get current url
     */
    public function getCurrentUrls()
    {
        return $this->_urlBuilder->getCurrentUrl();
    }

    public function getNameTitle()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_name_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPhoneTitle()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_phone_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getEmailTitle()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_email_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSubjectTitle()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_subject_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getDescriptionTitle()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_desc_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getDatePickerTitle()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_date_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getCheckboxTitle()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_checkbox_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFileTitle()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_file_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getNamePlaceholder()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_name_placeholder', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getEmailPlaceholder()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_email_placeholder', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPhonePlaceholder()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_phone_placeholder', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSubjectPlaceholder()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_subject_placeholder', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getDescriptionPlaceholder()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_desc_placeholder', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getDatePickerPlaceholder()
    {
        return $this->scopeConfig->getValue('contactwidget_section/contactformoptions/form_date_placeholder', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
