<?php
namespace Solwin\Contactwidget\Controller\Adminhtml\Contact;

class Delete extends \Solwin\Contactwidget\Controller\Adminhtml\Contact
{

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $model = $this->_objectManager->create('Solwin\Contactwidget\Model\Contactdata');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('You deleted the contact.'));
                $this->_redirect('solwin_contactwidget/*/');
                return;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError(
                    __('We can\'t delete item right now. Please review the log and try again.')
                );
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->_redirect('solwin_contactwidget/*/edit', ['id' => $this->getRequest()->getParam('id')]);
                return;
            }
        }
        $this->messageManager->addError(__('We can\'t find a contact to delete.'));
        $this->_redirect('solwin_contactwidget/*/');
    }
}
