<?php
namespace Solwin\Contactwidget\Controller\Adminhtml\Contact;

class Index extends \Solwin\Contactwidget\Controller\Adminhtml\Contact
{
    /**
     * Items list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Solwin_Contactwidget::test');
        $resultPage->getConfig()->getTitle()->prepend(__('Contact Data'));
        $resultPage->addBreadcrumb(__('Test'), __('Test'));
        $resultPage->addBreadcrumb(__('Contact'), __('Contact'));
        return $resultPage;
    }
}