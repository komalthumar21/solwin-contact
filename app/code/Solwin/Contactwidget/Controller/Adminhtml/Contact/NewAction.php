<?php
namespace Solwin\Contactwidget\Controller\Adminhtml\Contact;

class NewAction extends \Solwin\Contactwidget\Controller\Adminhtml\Contact
{

    public function execute()
    {
        $this->_forward('edit');
    }
}
