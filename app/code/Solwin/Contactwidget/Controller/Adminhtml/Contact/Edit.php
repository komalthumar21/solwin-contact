<?php
namespace Solwin\Contactwidget\Controller\Adminhtml\Contact;

class Edit extends \Solwin\Contactwidget\Controller\Adminhtml\Contact
{

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        
        $model = $this->_objectManager->create('Solwin\Contactwidget\Model\Contactdata');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This item no longer exists.'));
                $this->_redirect('solwin_contactwidget/*');
                return;
            }
        }
        // set entered data if was error when we do save
        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }
        $this->_coreRegistry->register('current_solwin_contactwidget_contact', $model);
        $this->_initAction();
        $this->_view->getLayout()->getBlock('solwin_contactwidget_contact_edit');
        $this->_view->renderLayout();
    }
}
