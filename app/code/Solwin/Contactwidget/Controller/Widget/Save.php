<?php
namespace Solwin\Contactwidget\Controller\Widget;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Solwin\Contactwidget\Model\Contactdata;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;

class Save extends Action
{
    protected $resultPageFactory;
    protected $contactdata;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Contactdata $contactdata
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->contactdata = $contactdata;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $data = $this->getRequest()->getPost();
            $data['features'] = implode(",",$data['features']);
            $data = json_decode((json_encode($data)),true);
            if (isset($data)) {
                $model = $this->contactdata;
                $model->setData($data)->save();
                $this->messageManager->addSuccessMessage(__("Data Saved Successfully."));
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __("We can\'t submit your request, Please try again."));
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;

    }
}
