<?php
namespace Solwin\Contactwidget\Block\Adminhtml\Contact\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('solwin_contactwidget_contact_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Contact'));
    }
}
