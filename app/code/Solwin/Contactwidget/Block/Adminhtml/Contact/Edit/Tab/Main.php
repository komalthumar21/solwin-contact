<?php
namespace Solwin\Contactwidget\Block\Adminhtml\Contact\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Main extends Generic implements TabInterface
{
    protected $_wysiwygConfig;
 
    public function __construct(
        \Magento\Backend\Block\Template\Context $context, 
        \Magento\Framework\Registry $registry, 
        \Magento\Framework\Data\FormFactory $formFactory,  
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig, 
        array $data = []
    ) 
    {
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Contact Data');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Contact Data');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_solwin_contactwidget_contact');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('contact_');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Contact Data')]);
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }
        $fieldset->addField(
            'name',
            'text',
            ['name' => 'name', 'label' => __('Name'), 'title' => __('Name'), 'required' => true]
        );
        $fieldset->addField(
            'email',
            'text',
            ['name' => 'email', 'label' => __('Email'), 'title' => __('Email'), 'required' => true]
        );
        $fieldset->addField(
            'telephone',
            'text',
            ['name' => 'telephone', 'label' => __('Telephone'), 'title' => __('Telephone'), 'required' => true]
        );
        $fieldset->addField(
            'subject',
            'text',
            ['name' => 'subject', 'label' => __('Subject'), 'title' => __('subject'), 'required' => true]
        );
        $fieldset->addField(
            'comment',
            'text',
            ['name' => 'comment', 'label' => __('Comment'), 'title' => __('Comment'), 'required' => true]
        );
        $fieldset->addField(
            'datetimepicker',
            'text',
            ['name' => 'datetimepicker', 'label' => __('Datetimepicker'), 'title' => __('Datetimepicker'), 'required' => true]
        );
        $fieldset->addField(
            'features',
            'text',
            ['name' => 'features', 'label' => __('Features'), 'title' => __('features'), 'required' => true]
        );
        $fieldset->addField(
            'title',
            'file',
            [
                'name' => 'file', 
                'label' => __('File label'),
                'required' => true
            ]
        );
        
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
