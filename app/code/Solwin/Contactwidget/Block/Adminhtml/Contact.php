<?php
namespace Solwin\Contactwidget\Block\Adminhtml;

class Contact extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'contact';
        $this->_headerText = __('Contact');
        parent::_construct();
    }
}
